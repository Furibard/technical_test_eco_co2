from django.db import models
from django.core.validators import MinValueValidator


class ConsumptionCO2(models.Model):
    datetime = models.DateTimeField()
    co2_rate = models.FloatField(validators=[MinValueValidator(0.0)])
    average_per_hour = models.ForeignKey('ConsumptionCO2AveragePerHour', on_delete=models.CASCADE)
    is_working_day = models.BooleanField()

    def __str__(self):
        return f"{self.id}: {self.datetime} {self.co2_rate}"


class ConsumptionCO2AveragePerHour(models.Model):
    co2_rate_average = models.FloatField(validators=[MinValueValidator(0.0)])

    def __str__(self):
        return f"{self.co2_rate_average}"