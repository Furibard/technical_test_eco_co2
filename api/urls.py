from django.urls import path

from . import views


urlpatterns = [
    path('', views.table, name='table'),
    path('graphic/', views.graphic, name='graphic'),
    path('display-graphic/', views.displayGraphic, name='display-graphic'),
    path('fetch-data/', views.fetchData, name='fetch-data'),
    path('delete-data/', views.deleteData, name='delete-data')
]