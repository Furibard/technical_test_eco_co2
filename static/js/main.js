// Récupère les données depuis l'API Eco CO2
$("#fetch-data").click(function() {
  $(this).prop("disabled", true);
  $(this).html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Chargement en cours...");

  $.get(`${window.location.origin}/fetch-data/`, function() {
    document.location.reload();
  })
});

// Supprime les données
$("#delete-data").click(function() {
  $(this).prop("disabled", true);
  $(this).html("<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Suppression en cours...");

  $.get(`${window.location.origin}/delete-data/`, function() {
    document.location.reload();
  })
});